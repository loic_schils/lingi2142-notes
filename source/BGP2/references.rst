.. References

.. [Huston06] Huston, Geoff. "Exploring autonomous system numbers." The Internet Protocol Journal 9, no. 1 (2006): 2-23.

.. [WikiBGP] Wikipedia contributors, "Border Gateway Protocol," Wikipedia, The Free Encyclopedia, http://en.wikipedia.org/w/index.php?title=Border_Gateway_Protocol&oldid=659671316 (accessed May 10, 2015).

.. [FBR03] Feamster, Nick, Jay Borkenhagen, and Jennifer Rexford. "Guidelines for interdomain traffic engineering." ACM SIGCOMM Computer Communication Review 33, no. 5 (2003): 19-30.

.. [Foster03] Foster, Kris. "Application of BGP communities." The Internet Protocol Journal 6, no. 2 (2003): 2-9.

.. [QPB05] Quoitin, Bruno, Cristel Pelsser, Olivier Bonaventure, and Steve Uhlig. "A performance evaluation of BGP-based traffic engineering." International journal of network management 15, no. 3 (2005): 177-191.

.. [QUP03] Quoitin, Bruno, Cristel Pelsser, Louis Swinnen, Ouvier Bonaventure, and Steve Uhlig. "Interdomain traffic engineering with BGP." Communications Magazine, IEEE 41, no. 5 (2003): 122-128.

.. [STR06] Sangli, S., Daniel Tappan, and Yakov Rekhter. "BGP extended communities attribute." draft-ietf-idr-bgp-ext-communities-05. txt 5, no. 02 (2006).

.. [RFC4271] Rekhter, Y. and Li, T. and Hares, S., 'A Border Gateway Protocol 4 (BGP-4)', https://tools.ietf.org/html/rfc4271 [ONLINE], 'RFC 4271' (Jan. 2006)