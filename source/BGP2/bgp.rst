BGP Routing on the Internet
---------------------------
.. sectionauthor:: Loïc Schils

Two protocols operate internet routing:

- Intradomain routing protocol: In an individual domain, intradomain routing protocol communicates the topology of the network to all the domain's routers. It also selects the shortest path to reach a particular destination. This selection is based on some metrics determined by the administrator. There are multiples intradomain routing protocols but it's mostly RIP (distance vector) and OSPF (link-state routing) that are used.
- Interdomain routing protocol: This protocol communicates information about reachability. For each destination, it picks the best route following policies information defined by each administrator. There is only one interdomain routing protocol used on the internet which is BGP.

BGP [RFC4271]_ is used in the interdomain routing protocol. A domain is called an Autonomous System (AS). There are two variants of BGP:

- iBGP (Interior Border Gateway Protocol): Propagate the best routes, learned from other ASes, to internal peers (inside an the same AS).
- eBGP (Exterior Border Gateway Protocol): It allows communication with external peers (from distinct ASes). Reachable prefixes are announced with eBGP.

Inside one domain, intradomain routing protocol communicates all known paths to all routers. However on the Internet, an AS will not provide a transit service to all routes. It is possible to select route advertisements destined to router outside of the domain.

We recall briefly the BGP decision process. There are 3 steps:

1. Input filter: The input filter is defined by the administrator. It is used to filter advertisements. The accepted route advertisements are placed in the BGP routing table (before the placement, some attributes can be updated).

2. Selection of the best route: It is the selection of the best route toward each known network. The decision is made following these rules:

	A. Remove unreachable nexthop
 	B. Highest LOCAL_PREF
 	C. Shortest AS_PATH
 	D. Lowest MED
 	E. eBGP over iBGP
 	F. Nearest IGP neightbor
 	G. Smallest router ID

3. Output filter: It is the selection of route that will be communicated to other BGP peers. No more than one route can be communicated for each destination.
