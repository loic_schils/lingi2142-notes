BGP Communities
---------------
.. sectionauthor:: Olivier Nauw

A community is an attribute of a prefix in the BGP protocol, the BGP community attribute. This attribute is a tag that can be applied to incoming or outgoing prefixes to achieve some common goal, like filter, know the origin, set local preference, etc. Communities can be used to mark a set of prefixes that share a common property. "It is a transitive optional attribute, meaning BGP implementations do not have to recognize it and at the network, operator’s discretion carry it through an AS or pass it to another AS" [Foster03]_. This attribute allow to influence the routing inside another AS. It is a clean and consistent ways to manage and control the traffic.

The BGP community attribute is a 32 bits value, so four bytes. The first two bytes represent the AS number (ASN) and the last two bytes represent a value with a predetermined meaning. The simplest manner and the most used is to display communities as *ASN:VALUE*.

.. figure:: figures/BGP2_CommunityAttr.png
   :align: center
   :scale: 70   

   Community Formats (taken from [Foster03])

We can notice that there are three main type of exchange of prefix: 

	- Provider-customer relation: The customers pay to receive all the prefix of the provider.
	- Customer-provider relation: The customers send only the prefix they own to the provider.
	- Peers relation: In this case, they both send only their customer prefix and not the other peers prefix.

Three Communities in RFC1997
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	- NO-EXPORT: Do not advertise this route to external BGP peers (eBGP).
	- NO-ADVERTISE: Do not advertise this route to any peer, internal or external.
	- NO-ADVERTISE-SUBCONFED: Prevent a prefix from being advertised to other members within a confederation (it can be multiple sub-ASes) but the use of the confederation is rare.

	- NO-PEER (proposed in an internet draft): Constraint propagation only to transit providers and not peers.

Extended Communities
~~~~~~~~~~~~~~~~~~~~

We can go further than these policies with a new transitive optional attribute called the *extended community*. This new attribute is a 8 bytes value like depicted in the figure above. The first bytes specify the type. It is this value which dictates the remaining of the attribute and give a certain flexibility. The non-transitive attribute means that it should be send only inside the local AS.

Some example are available in an internet draft [STR06]_ like the *route target community* with which we can identifies a set of router that may receive this prefix and the *link bandwidth community* to influence the best path selection.

Examples
~~~~~~~~

**1.** First example of the uses of community is the control of the policies of the three basic type of relationship. As shown in the first figure below, the customers prefixes are tagged with 53:100, peers have the tag 53:200 and transit 53:300. With these communities, we can apply the rule like the provider-customer relation. The customer-facing BGP session is configured to send all prefixes matching the 53:100, 53:200 and 53:300 communities.

.. figure:: figures/BGP2_CommunityEx1.png
   :align: center
   :scale: 70   

   Internal Use of Communities for Applying a Basic Service Provider Policy (taken from [Foster03]) 
	
In addition to these policies we can add more details such as the geographical information of the route. A value can be assigned to each country, city, etc. Another details that we can would to add to a prefix is to specify the original protocol (OSPF, IS-IS, etc.). This help us to quickly knows where a prefix came without tracing it back to the point of its origination. Moreover, it exists two manners for adding these new policies to the three basics. Simply add new code and keep one signification for one code (53:1 for east coast prefix) or we can mix them like 53:101 for a prefix of a customer of the east coast.


**2.** We can mention the case of a customers that want to manage a primary and a backup circuit, as if the backup link cost much than the primary. "Two simple communities can be used to effectively influence a service provider into using the appropriate primary and backup circuits: one value to lower and another to raise the preference of specific prefixes during the transit provider's best-path selection." [Foster03]_

**3.** With a community, we can request that the neighbor drop all traffic to a prefix as illustrated in the figure below. A Denial-of-Service attacks can target an entire network or one or more host. In this case, we can use the community attribute to filter (= drop all) the traffic on the destination address. To do this, the provider have an address on the BGP routers that route to the NULL interface (point 3 in the figure below). When the customer send a prefix with a community for blackhole it, the NEXT_HOP is changed to the address that the provider choose to be routed toward the NULL interface (point 2 in the second figure below). So, all the traffic with 192.168.1.1 (the blackholed address) as destination address will be discarded at NULL interface.

.. figure:: figures/BGP2_CommunityEx3.png
   :align: center
   :scale: 100

   Customer-Initiated Black Hole to Defend Against a DoS Attack (taken from [Foster03])

**4.** When the providers send the communities to the customers. The providers can tag the type of the prefix and the other networks can select more specific address. 

For example, if a community says that a prefix is a more specific block within a Classless Inter-Domain Routing (CIDR) block, the customer can choose to accept this more specific prefix.

Network A announces :

	- 142.77.0.0/16 with a tag of 1:77 (1:77 means it is a CIDR block)
	- 142.77.1.0/24 with a tag of 1:88 (1:88 means it is a more specific block within a CIDR block)
	- 150.3.12.0/24 with a tag of 1:99 (1:99 means that the full CIDR block is not being announced)

Network B then has the option of accepting the more specific 142.77.1.0/24. It also knows that it must accept 150.3.12.0/24 because there is no other route to this network.

This last example is taken from [Foster03]_.
