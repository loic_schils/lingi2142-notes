Characteristics of Interdomain Traffic
--------------------------------------
.. sectionauthor:: Loïc Schils

Interdomain traffic engineering is significantly more complicated than intradomain traffic engineering.

There are some observations on the paper [QUP03]_:

- Each ISP exchanges IP Packets with a large fraction of the Internet (during just a few days).
- ISPs only exchange a small fraction of their traffic with their direct peer (AS-hop distance of 1). The majority of the source destination of interdomain traffic is a few AS-hop away.
- Only a small number of ASes are resposible for a large part of the interdomain traffic.

With these observations we can deduce that:

- AS willing to engineer its interdomain could move a larger amount of traffic by influencing a small number of distant ASes.
- Interdomain traffic engineering solutions should be able to influence ASes a few hops beyonnd their upstream providers or direct peers.