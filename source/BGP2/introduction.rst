Introduction
------------
.. sectionauthor:: Loïc Schils, Simon Thibert

At the beginning, the Internet was build to support a small number of research networks. Nowadays, the Internet has become a huge network supporting critical applications distributed around the world. This brings some new challenges.

The first one is the size of the Internet. There are more than one billion of hosts shared between thousands of different domains. A domain generally corresponds to one company or one Internet Service Provider (ISP). These domains are interconnected with each other. Border Gateway Protocol (BGP) handles the routing of IP Packets exchanged between these domains.

We may encounter two different types of domains:

- Stub domain: In this domain, hosts produce or consumne IP Packets but do nothing with IP Packets that are not produced or destined to this domain.
- Transit domain: It interconnects different domains together and carries IP packets produced or destined to external domains.

The second challenge is that the Internet was developped with the idea that connectivity was the most important problem. Today this connectivity has been achieved, the challenge is now to provide mission critical applications with tight Service Level Agreements (SLA). To reach these requirements, traffic engineering is used to control the flow of IP packets inside their own domain and thus reduce congestion. Several techniques can be used by ISPs. Some rely on Mutli-Protocol Label Switching (MPLS) and other rely on a tuning of IP routing protocols which is used inside the ISP network. But ISPs also need to control the flow of their interdomain traffic. Here are three examples to show that interdomain routing is an important part of traffic engineering [FBR03]_:

	- Congested edge link: Links between domains being common points of congestion in the Internet, a network operator can, upon detecting an overloaded edge link, change the interdomain paths to direct some of the traffic to a less congested link.

	- Upgraded link capacity: Operators of large IP backbones frequently install new, higher-bandwidth links between domains. Exploiting the additional capacity may require routing changes that divert traffic traveling via other edge links to the new link.

	- Violation of peering agreement: An AS pair may have a business arrangement that restricts the amount of traffic they exchange.

MPLS is not used between interdomain boundaries, therefore ISPs can only rely on tuning BGP routing policies. But BGP was not designed with traffic engineering in mind, which make this a bit tricky. It is done by trial-and-error and there are some limitations.

In the remainder of this chapter, we first remind concepts of BGP routing on the Internet. The second section summarizes the main characteristics of interdomain traffic. Then, we survey some specific guidelines for interdomain traffic engineering. Finally, we explain the role of BGP communities.
