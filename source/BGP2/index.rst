.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Traffic engineering with Border Gateway Protocol
================================================

This chapter will describe traffic engineering techniques with the Border Gateway Protocol. Such techniques are used to better control the flow of packets inside an IP network. This chapter is based on the following papers [QUP03]_, [Foster03]_, [QPB05]_, [FBR03]_, [STR06]_.

.. toctree::
    
    introduction
    bgp
    characteristics
    high-level_overview
    guidelines
    bgp_communities
    conclusion
    exercices
    references