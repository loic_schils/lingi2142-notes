.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

IPv6 Multicast
==============

This chapter will describe how IPv6 networks support Multicast.