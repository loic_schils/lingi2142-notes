.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

MPLS Traffic Engineering
========================

This chapter will describe how Traffic Engineering can be deployed in MPLS networks.